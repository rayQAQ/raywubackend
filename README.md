## api sample with swagger and jwt

# Start

check root folder named raywubackend
check GO installed
check project on $GOPATH/src
check $PATH set on \$GOPATH/bin

run
$ go get -d
$ go run main.go

# Swagger test

1. start project or run 'go build' and double click .exe

2. if not installed go-swagger
   \$ go get github.com/go-swagger/go-swagger

3. generate swagger.json
   \$ swagger generate spec -o ./swagger.json

4. go to https://editor.swagger.io/ and selete file=>import file => import swagger.json

# Swagger api test remark

## query/update need to set JWT token

get token from api SignIn,and copy token
click [Authorize] value input, type "Bearer your_token" click [Authorize] again

api response object sample
{"code": status code,"message": response message,"data": response data}
