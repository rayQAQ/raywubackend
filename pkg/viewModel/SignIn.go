package viewModel

// swagger:parameters signInRequest
type SignInRequestBody struct {
	// in: body
	Body SignInRequest
}

type SignInRequest struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}

type SignInResponse struct {
	Token string `json:"token"`
}
