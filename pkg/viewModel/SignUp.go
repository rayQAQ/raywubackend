package viewModel

// swagger:parameters SignUpRequest
type SignUpRequestBody struct {
	// in: body
	Body SignUpRequest
}

type SignUpRequest struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}
