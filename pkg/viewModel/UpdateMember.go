package viewModel

// swagger:parameters SignUpRequest
type UpdateMemberRequestBody struct {
	// in: body
	Body UpdateMemberRequest
}

type UpdateMemberRequest struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}
