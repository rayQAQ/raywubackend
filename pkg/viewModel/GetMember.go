package viewModel

type GetMemberResponse struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}
