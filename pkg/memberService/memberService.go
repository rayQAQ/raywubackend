// Package memberService API.
//
// The purpose of this service is to provide an application
// that is using plain go code to define an API
//
//      Schemes: http, https
//
//      securityDefinitions:
//        Bearer:
//           type: apiKey
//           name: Authorization
//           in: header
//
//      Host: localhost:8080
//      Version: 1.0.0
//
// swagger:meta
package memberService

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"rayWuBackend/pkg/memberDb"

	vm "rayWuBackend/pkg/viewModel"

	"github.com/robbert229/jwt"
)

func SignUp(w http.ResponseWriter, r *http.Request) {
	// swagger:route POST /SignUp Member SignUpRequest
	//
	// 註冊
	//
	// 驗證帳密建立會員
	//
	//     Responses:
	//       200: ResponseBody

	body, _ := ioutil.ReadAll(r.Body)
	var req vm.SignUpRequest

	if err := json.Unmarshal(body, &req); err != nil {
		fmt.Println(err)
		panic(err)
	}

	if result := memberDb.InsertMember(req); result {
		writeResponse(w, 0, "註冊成功!", nil)
	} else {
		writeResponse(w, 1000, "註冊失敗!", nil)
	}
}

func SignIn(w http.ResponseWriter, r *http.Request) {
	// swagger:route POST /SignIn Member signInRequest
	//
	// 登入
	//
	// 驗證帳密發token
	//
	//     Responses:
	//       200: ResponseBody

	body, _ := ioutil.ReadAll(r.Body)
	var req vm.SignInRequest

	if err := json.Unmarshal(body, &req); err != nil {
		fmt.Println(err)
		panic(err)
	}

	if result := memberDb.ValidateLogin(req); result {
		token := generatorJWT(req.Account)
		res := vm.SignInResponse{Token: token}

		// validateJWT(token)

		writeResponse(w, 0, "登入成功!", res)
	} else {
		writeResponse(w, 1000, "登入失敗!", nil)
	}
}

func GetMember(w http.ResponseWriter, r *http.Request) {
	// swagger:route POST /GetMember Member noRequest
	//
	// 查詢會員資料
	//
	// 根據token取會員資料
	//
	// Security:
	//- Bearer:
	//
	//     Responses:
	//       200: ResponseBody

	reqToken := r.Header.Get("Authorization")
	if reqToken == "" {
		writeResponse(w, 1000, "No Authorization Bearer", nil)
		return
	}

	// fmt.Printf("get Authorization=%s", reqToken)
	splitToken := strings.Split(reqToken, "Bearer")
	reqToken = splitToken[1]

	ok, account := validateJWT(reqToken)
	if !ok {
		writeResponse(w, 1000, "Invalid Token", nil)
		return
	}

	res := memberDb.SelectMember(account)
	writeResponse(w, 0, "查詢成功", res)
}

func UpdateMember(w http.ResponseWriter, r *http.Request) {
	// swagger:route POST /UpdateMember Member updateMemberRequest
	//
	// 更新會員資料
	//
	// 根據token更新會員資料
	//
	// Security:
	//- Bearer:
	//
	//     Responses:
	//       200: ResponseBody

	reqToken := r.Header.Get("Authorization")
	if reqToken == "" {
		writeResponse(w, 1000, "No Authorization Bearer", nil)
		return
	}

	// fmt.Printf("get Authorization=%s", reqToken)
	splitToken := strings.Split(reqToken, "Bearer")
	reqToken = splitToken[1]

	ok, _ := validateJWT(reqToken)
	if !ok {
		writeResponse(w, 1000, "Invalid Token", nil)
		return
	}

	body, _ := ioutil.ReadAll(r.Body)
	var req vm.UpdateMemberRequest

	if err := json.Unmarshal(body, &req); err != nil {
		fmt.Println(err)
		panic(err)
	}

	if result := memberDb.UpdateMember(req); result {
		writeResponse(w, 0, "更新成功!", nil)
	} else {
		writeResponse(w, 1000, "更新失敗!", nil)
	}
}

func writeResponse(w http.ResponseWriter, code int, message string, data interface{}) {
	// w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Access-Control-Allow-Origin", "*")
	// w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	// w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	// w.Header().Set("Access-Control-Allow-Headers", "Accept, Accept-Language, Content-Type")
	resp := ResponseMessage{Code: code, Message: message, Data: data}
	b, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, err)
		// logrus.Warnf("error when marshal response message, error:%v\n", err)
	}
	w.Write(b)
}

func generatorJWT(userName string) string {
	secret := "ThisIsMySuperSecret"
	algorithm := jwt.HmacSha256(secret)

	claims := jwt.NewClaim()
	claims.Set("Role", userName)
	claims.SetTime("exp", time.Now().AddDate(1, 0, 0))

	token, err := algorithm.Encode(claims)
	if err != nil {
		panic(err)
	}

	return token
}

func validateJWT(tokenString string) (status bool, account string) {
	secret := "ThisIsMySuperSecret"
	algorithm := jwt.HmacSha256(secret)
	// if err := algorithm.Validate(tokenString); err != nil {
	// 	panic(err)
	// }

	loadedClaims, err := algorithm.Decode(tokenString)
	if err != nil {
		panic(err)
	}

	role, err := loadedClaims.Get("Role")
	if err != nil {
		panic(err)
	}

	roleString, ok := role.(string)
	if !ok {
		panic(err)
	}

	return ok, roleString
}

// Success
//
// swagger:response ResponseBody
type ResponseBody struct {
	// in: body
	Body ResponseMessage
}

type ResponseMessage struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
