package memberDb

import (
	vm "rayWuBackend/pkg/viewModel"
)

func InsertMember(req vm.SignUpRequest) bool {
	return true
}

func ValidateLogin(req vm.SignInRequest) bool {
	return true
}

func SelectMember(account string) vm.GetMemberResponse {
	return vm.GetMemberResponse{Name: account, Age: 30}
}

func UpdateMember(req vm.UpdateMemberRequest) bool {
	return true
}
