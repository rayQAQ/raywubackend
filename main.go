//go:generate swagger generate spec
package main

import (
	"log"
	"net/http"
	"rayWuBackend/pkg/memberService"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/SignUp", memberService.SignUp).Methods("POST")
	r.HandleFunc("/SignIn", memberService.SignIn).Methods("POST")
	r.HandleFunc("/GetMember", memberService.GetMember).Methods("POST")
	r.HandleFunc("/UpdateMember", memberService.UpdateMember).Methods("POST")

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"https://editor.swagger.io"},
		// AllowedMethods:   []string{"POST", "GET", "OPTIONS", "PUT", "DELETE"},
		AllowedHeaders:   []string{"Content-Type", "Authorization"},
		AllowCredentials: true,
		Debug:            true,
	})

	handler := c.Handler(r)

	if err := http.ListenAndServe(":8080", handler); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
